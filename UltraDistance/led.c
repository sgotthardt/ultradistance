#include <avr/io.h>


#include "led.h"
#include "timeout.h"




// for this to work 'nicely' all leds should be on the same port

#define LED_PORT PORTC
#define LED_RED_PIN    (1 << 2)
#define LED_GREEN_PIN  (1 << 0)
#define LED_BLUE_PIN   (1 << 1)
#define LED_ALL_PINS  (LED_RED_PIN | LED_GREEN_PIN | LED_BLUE_PIN)


static TimeoutT led_timeout;
static uint8_t  led_blinking;
static uint8_t  led_on;




// call this from main to keep it in the loop
void LedScan(void)
{
	if (led_blinking)
	{
		if (TimeoutOneshot(&led_timeout, 0.17f))
		{
			PORTC ^= led_blinking;
			TimeoutClear(&led_timeout);
		}
	} else TimeoutClear(&led_timeout);
}



void LedOn(enum LED led)
{
	uint8_t on = 0;

	switch (led)
	{
		case LED_RED:   on = LED_RED_PIN;
		break;
		
		case LED_GREEN: on = LED_GREEN_PIN;
		break;
		
		case LED_BLUE:  on = LED_BLUE_PIN;
		break;		
	}
	
	if (on != led_on)
	{
		LedOff();
		led_on = on;
		LED_PORT &= ~on;
	}
}



// start an LED blinking
void LedBlink(enum LED led)
{
	uint8_t blinker = 0;
	
	switch (led)
	{
		case LED_RED:
		blinker = LED_RED_PIN;
		break;
		
		case LED_GREEN:
		blinker = LED_GREEN_PIN;
		break;
		
		case LED_BLUE:
		blinker = LED_BLUE_PIN;
		break;
	}
	
	// was there a change? record and reset
	if (blinker != led_blinking)
	{
		LedOff();
		led_blinking = blinker;
	}
}




// set ALL LEDS off
void LedOff(void)
{
	// make sure we are output pins
	DDRC |= LED_ALL_PINS;
	
	led_on = 0;
	led_blinking = 0;
	LED_PORT |= LED_ALL_PINS;
}
