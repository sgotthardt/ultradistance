#include <avr/io.h>


void NvStore(uint16_t address, uint8_t data)
{
	// wait for any access to finish
	while (EECR & (1 << EEPE))  ;

	EEAR = address;
	EEDR = data;

	EECR |= (1 << EEMPE);
	EECR |= (1 << EEPE);
}



uint8_t NvRecall(uint16_t address)
{
	// wait for any access to finish
	while (EECR & (1 << EEPE))  ;

	EEAR = address;
	EECR |= (1 << EERE);
	return EEDR;
}