#ifndef _DISTANCE_H_
#define _DISTANCE_H_

#define DISTANCE_TOO_FAR (0xFFFF)
#define DISTANCE_MAX_INCHES (150)
#define DISTANCE_VALID_INCHES (100)


// divided down to inches from sensor head
int16_t DistanceInches(void);

// raw reading
uint16_t DistanceRaw(void);

// initialize the sensor - starts running and measuring automatically
void DistanceInit(void);

// returns non-zero for error
int DistanceOutOfRange(void);



#endif