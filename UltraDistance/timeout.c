#include <avr/io.h>

#include "timeout.h"
#include "main.h"



// main.h should have MainCounter() and T2_IPS

// Rare problems:
// count can never be zero and be running
// overflow occurs every year


// start running the timeout with the given seconds
void TimeoutStart(TimeoutT *timeout, float seconds)
{
	timeout->count = MainCounter() + T2_IPS * seconds;
}



// checks timeout and starts it if not running
// returns true if finished
int TimeoutOneshot(TimeoutT *timeout, float seconds)
{
	if (timeout->count)
	{
		return MainCounter() >= timeout->count;
	}

	TimeoutStart(timeout, seconds);
	
	return 0;
}



// returns true if the timeout has passed
inline int TimeoutIsFinished(TimeoutT *timeout)
{
	return (timeout->count) && (MainCounter() >= timeout->count);
}




// clears running status
void TimeoutClear(TimeoutT *timeout)
{
	timeout->count = 0;
}

