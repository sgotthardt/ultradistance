#ifndef _MAIN_H_
#define _MAIN_H_


#define ADC_MUX_ADC0 0
#define ADC_MUX_ADC1 1
#define ADC_MUX_ADC2 2
#define ADC_MUX_ADC3 3
#define ADC_MUX_ADC4 4
#define ADC_MUX_ADC5 5
#define ADC_MUX_ADC6 6
#define ADC_MUX_ADC7 7
#define ADC_MUX_BGAP 14
#define ADC_MUX_GND  15


#define ADC_SENSE_V ADC_MUX_ADC0
#define ADC_SENSE_T ADC_MUX_ADC1
#define ADC_SENSE_1 ADC_MUX_ADC2
#define ADC_SENSE_2 ADC_MUX_ADC3

// how many inches per second
#define SPEED_OF_SOUND (1125 * 12)


// interrupts per second for Timer2
#define T2_IPS 125


// return system interrupts counter
uint32_t MainCounter(void);



#endif
