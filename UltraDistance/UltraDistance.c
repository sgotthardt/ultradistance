#include <stdint.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "main.h"
#include "distance.h"

// COUNTS for the T1 count clock frequency for capture
#define COUNTS_PER_SECOND (F_CPU / 8L)
#define COUNTS_PER_INCH (COUNTS_PER_SECOND / SPEED_OF_SOUND)

#define T0_IPS  250	// divides nicely into 8MHz 256 and 125 and 250

// phase measured can't go above 65500 counts because of roll over
// cooled is time between measurements
#define MEASUREMENT_INTERVAL (T0_IPS * 60000 / COUNTS_PER_SECOND)


#define DISTANCES_NUM 4
static int8_t dist_index;
static volatile uint32_t distance_total;
static volatile uint16_t distances[DISTANCES_NUM];

static volatile uint16_t distance;
static volatile int distance_error;





static volatile enum
{
	STATE_TRIGGERED,
	STATE_ECHO,
	STATE_FINISHED,
} machine_state = STATE_FINISHED;



int DistanceOutOfRange(void)
{
	return distance_error;
}



int16_t DistanceInches(void)
{
	return (int16_t)(distance_total / DISTANCES_NUM / COUNTS_PER_INCH / 2);
}



uint16_t DistanceRaw(void)
{
	return distance;
}



// trigger pin is 2; our choice. Capture pin is HW bound at Port B.0
#define TRIGGER_BP (1 << 2)
#define CAPTURE_BP (1 << 0)

static void Ultra_Trigger(void)
{
	// set bit so the interrupt happens when the pulse goes high 
	TCCR1B |= (1<<ICES1);
	
	// clear any interrupt that may be left over
	TIFR1 |= (1<<ICF1);

	// re-enable interrupt for capture
	TIMSK1 |= (1 << ICIE1);
	
	// trigger the ultrasonic
	DDRB  |= TRIGGER_BP;
	PORTB |= TRIGGER_BP;
	_delay_us(10);
	PORTB &= ~(TRIGGER_BP);
}




// Capture interrupt happens when the input goes high or low
ISR(TIMER1_CAPT_vect)
{
	static uint16_t echo_start;

	switch (machine_state)
	{	
		case STATE_TRIGGERED:
		echo_start = ICR1;
		// clear the bit to look for a falling edge now
		TCCR1B &= ~(1<<ICES1);
		machine_state = STATE_ECHO;
		break;	
		
		case STATE_ECHO:
		// no more capture interrupts
		TIMSK1 &= ~(1 << ICIE1);
		
		// save raw distance in ticks
		distance = (ICR1 - echo_start);
		machine_state = STATE_FINISHED;
		break;
		
		default: break;
	}
}



// Timer interrupt for setting measurement interval
// INTS_PER_SECOND
ISR(TIMER0_COMPA_vect)
{
	static uint8_t phase;
	
	// check for time to start measurement - every 60mS
	if (++phase < MEASUREMENT_INTERVAL) return;
	phase = 0;
	
	// The measurement should have finished by now
	//   if not, the sensor is missing or the distance is too far
	// The interval should be small enough so the 16 bit register wont overflow
	if (machine_state != STATE_FINISHED)
	{
		distance_error = DISTANCE_TOO_FAR;
	}
	else
	{
		// fill the queue and average
		distance_total -= distances[dist_index];
		distances[dist_index] = distance;
		distance_total += distance;
		if (++dist_index >= DISTANCES_NUM) dist_index = 0;
		distance_error = 0;
	}
	
	// trigger a new measurement
	machine_state = STATE_TRIGGERED;
	Ultra_Trigger();
}



// call this and the sensor starts working
void DistanceInit(void)
{
	// Set timer 0 for interrupt
	TCCR0A = (1 << WGM01);		// CTC
	TCCR0B = 0x04;				// clock divisor selection
	TIMSK0 = 0x02;
	OCR0A = F_CPU / 256 / T0_IPS;

	// Setup the input capture on timer 1
	TCCR1A = 0;
	TCCR1B = (1 << ICNC1) | (0 << CS12) | (1 << CS11) | (0 << CS10);
	
	// initialize state and phase variables
	machine_state = STATE_FINISHED;
}
