#ifndef _LED_H_
#define _LED_H


// The LED array can only have one LED on at a time
// whenever an led is turned on, the others are turned off


// these values should equal the bit positions on the port where LED connects
enum LED
{
	LED_RED,
	LED_GREEN,
	LED_BLUE,
};


enum LED_STATE
{
	LED_OFF,
	LED_ON,
//	LED_BLINK,
};



// call from main
void LedScan(void);

void LedOn(enum LED led);
void LedBlink(enum LED led);
void LedOff(void);


#endif
