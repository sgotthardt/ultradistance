// main
#include <avr/interrupt.h>
#include <stdio.h>

#include "led.h"
#include "main.h"
#include "rs232.h"
#include "distance.h"
#include "moving.h"
#include "timeout.h"



static volatile uint16_t distance_setpoint;
static volatile uint32_t main_counter;



uint32_t MainCounter(void)
{
	return main_counter;
}



#define BUTTON_PIN (1 << PIND6)

// returns true if button is asserted
static int ButtonCheck(void)
{
	// check if pin is low - enable pull up
	PORTD |= BUTTON_PIN;
	return !(PIND & BUTTON_PIN);
}




static void Debug_Toggle(void)
{
	DDRD |=  0x80;
	PORTD ^= 0x80;
}



static void Distance_Report(void)
{
	char output[44];

	RS232_Clearscreen();
	RS232_CursorHome();

	if (DistanceOutOfRange() == 0)
	{
//		sprintf(output, "Distance - raw:%d, inches:%d\r\n", distance, (uint16_t)(distance / COUNTS_PER_INCH / 2));
		sprintf(output, "Distance - raw:%d, inches:%d, Check:%d\r\n", DistanceRaw(), DistanceInches(), ButtonCheck());
		RS232_Send(output);
	}
	else RS232_Send("Too Far\r\n");
}




// this could be interrupt driven
void Tuning(void)
{
	char rx;
	if ( (rx = RS232_Rx()) )
	{
		switch (rx)
		{
			case ' ':

			// other key?
			default: break;
		}
	}
}



// timer 2 interrupt - T2_IPS - Timer2 Interrupts Per Second
ISR(TIMER2_COMPA_vect)
{
	main_counter++;
}



__attribute__ ((OS_main)) int main(void)
{
	static TimeoutT button_timeout;
	static TimeoutT report_timeout;
	
	// Set timer 2 for interrupt with clearing match
	TCCR2A = (1 << WGM21);		// CTC
	TCCR2B = 0x06;				// clock divisor selection
	TIMSK2 = (1 << OCIE2A);
	OCR2A = F_CPU / 256 / T2_IPS;

	// make all the setup happen
	DistanceInit();
	MovingInit();
	RS232_Init();
	
	
	// enable interrupts - here we go!
	sei();

	for (;;)
	{
		// change stuff
		Tuning();
		
		// make LEDs work
		LedScan();

		// make the moving engine work
		MovingScan();

		// button pressed for setting distance?
		if (ButtonCheck())
		{
			if (TimeoutOneshot(&button_timeout, 0.5f) && 
			   !DistanceOutOfRange() && 
			    DistanceInches() < DISTANCE_VALID_INCHES)
			{
				MovingSetpoint(DistanceInches());
			}
		} else TimeoutClear(&button_timeout);

		// output stuff
		if (TimeoutOneshot(&report_timeout, 0.2f))
		{
			Distance_Report();			
			TimeoutClear(&report_timeout);
		}
		
	}

}