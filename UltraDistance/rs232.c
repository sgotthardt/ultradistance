
#include <avr/io.h>


#include "rs232.h"


// special character to setup control sequences
#define SEND_CSI do {RS232_Tx(0x9B);} while (0)



// low level transmit - one char
 void RS232_Tx(const uint8_t data)
{
	// wait for buffer to empty
	while ( !(UCSR0A & (1 << UDRE0)) )  ;

	// put in data to transmit
	UDR0 = data;
}



// peripheral clock is 1MHz
void RS232_Init(void)
{
	UCSR0B = (1 << TXEN0) | (1<< RXEN0);		// enable USART receiving and transmitting
	UCSR0C = 0x6;			// establish frame size of 8 bits
	UCSR0A = 0x02;			// double speed
	UBRR0  = 8;
	PORTC |= 1;		// pull up
}




// output the zero terminated string
void RS232_Send(const char *data)
{
	char out;

	while ( (out = *data++) )
	{
		RS232_Tx(out);
	}
}




void RS232_Clearscreen(void)
{
	SEND_CSI;
	RS232_Tx('2');
	RS232_Tx('J');
}



void RS232_Foreground(int color)
{
	SEND_CSI;
	RS232_Tx('3');
	RS232_Tx(color);
	RS232_Tx('m');
}



void RS232_Background(int color)
{
	SEND_CSI;
	RS232_Tx('4');
	RS232_Tx(color);
	RS232_Tx('m');
}



void RS232_CursorHome(void)
{
	SEND_CSI;
	RS232_Tx('H');
}



void RS232_Linefeed(void)
{
	RS232_Tx('\r');
	RS232_Tx('\n');
}



// return key or \0
uint8_t RS232_Rx(void)
{
	if (UCSR0A & (1 << RXC0))
	{
		return UDR0;
	}

	return '\0';
}

