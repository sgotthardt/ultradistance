#include <avr/io.h>

#include "nv.h"
#include "led.h"
#include "moving.h"
#include "timeout.h"
#include "distance.h"



static TimeoutT moving_timeout;
static int16_t moving_setpoint;
static TimeoutT outofrange_timeout;



#define EEPROM_SETPOINT (0x10)



void MovingInit(void)
{
	// read the setpoint from memory and check for sanity
	moving_setpoint = NvRecall(EEPROM_SETPOINT);
	if (moving_setpoint > 100 || moving_setpoint < 6) moving_setpoint = 12;
}




void MovingSetpoint(int16_t setpoint)
{
	// only update/store if setpoint has changed
	if (setpoint != moving_setpoint)
	{
		moving_setpoint = setpoint;
		NvStore(EEPROM_SETPOINT, (uint8_t)setpoint);
	}
}



// called with inches
void MovingScan(void)
{
	static int16_t distance_old;

	// the sensor may not get a good reading - wait if needed
	if (DistanceOutOfRange())
	{
		if (TimeoutOneshot(&outofrange_timeout, 1.0f))
		{
			LedOff();
			distance_old = 0;			
		}
		return;
	} else TimeoutClear(&outofrange_timeout);

	int16_t distance = DistanceInches();
	
	// first time through with legit distance?
	if (distance_old == 0)
	{
		distance_old = distance;
		return;
	}
	
	// how fast are we moving and in which direction?
	int16_t velocity = distance - distance_old;
	distance_old = distance;

	// closing in?
	if (velocity < 0)
	{
		TimeoutClear(&moving_timeout);
		
		if (distance <= moving_setpoint + 2)		// STOP
		{
			LedOn(LED_RED);
		}
		else if (distance < moving_setpoint + 24)	// getting there
		{
			LedBlink(LED_GREEN);
		}
		else if (distance < DISTANCE_MAX_INCHES)	// in sensor range
		{
			LedOn(LED_GREEN);
		}
	}
	else   	// car is stopped or backing up now
	{
		if (TimeoutOneshot(&moving_timeout, 3.0))
		{
			LedOff();
		}
	}
}




