#ifndef _MOVING_H_
#define _MOVING_H_



// call with distance periodically
void MovingScan(void);

// call with inches
void MovingSetpoint(int16_t setpoint);

// call to setup, etc.
void MovingInit(void);




#endif
