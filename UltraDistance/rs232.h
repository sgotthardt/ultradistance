#ifndef RS232_H
#define RS232_H


// call once and first
void RS232_Init(void);

// screen control
void RS232_Clearscreen(void);
void RS232_CursorHome(void);
void RS232_Linefeed(void);


// zero terminated string to send
void RS232_Send(const char *data);

// get the char from RS232 receive - returns '\0' if no key
uint8_t RS232_Rx(void);
void RS232_Tx(const uint8_t data);


// color control
#define XTERM_COLOR_BLACK    '0'
#define XTERM_COLOR_RED      '1'
#define XTERM_COLOR_GREEN    '2'
#define XTERM_COLOR_YELLOW   '3'
#define XTERM_COLOR_BLUE     '4'
#define XTERM_COLOR_MAGENTA  '5'
#define XTERM_COLOR_CYAN     '6'
#define XTERM_COLOR_WHITE    '7'
#define XTERM_COLOR_DEFAULT  '9'


void RS232_Foreground(int xcolorcode);
void RS232_Background(int xcolorcode);


#endif