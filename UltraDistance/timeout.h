#ifndef _TIMEOUT_H_
#define _TIMEOUT_H_


typedef struct TimeOut
{
	volatile uint32_t count;
} TimeoutT;


// set the time and start running the timeout
void TimeoutStart(TimeoutT *timeout, float seconds);

// returns true if the timeout has expired
int TimeoutIsFinished(TimeoutT *timeout);

// clear the timer to be used as a oneshot
void TimeoutClear(TimeoutT *timeout);

// will start the timer if not running - returns true if time is up
int TimeoutOneshot(TimeoutT *timeout, float seconds);


#endif

